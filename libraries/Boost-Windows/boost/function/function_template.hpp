�  Boost.F unction  library 

//  C opyright  Douglas  Gregor �2001- 6NEmil Xtch�evskiP7F Use, mod ifica�an d distribuis su bject to the� So ftware LicenGVerBs�1.0.^( See acco mpanying fil 1ICE NSE_1_0.Ptxt  �c � at6http:�//www.b��org/L)�F mo Cinf�ormb, s�;��"Note:�e�mheader 	�a �templat�\��mus t NOT ha ve multi ple-incl�u�v�Xpr (� |B#�de <^/f��/detail/ logue�.hpp>
�no_excep�`s_support�f d efined(B OOST_MSVC�D#   pr�agma  gn�\�( push �	�zab�d: 41 27 ) @�"condial �expres�u�H�@stant"� �e 	f�� �U#$ �#FUNCTI ON_TEMPL ATE_PARMSDPP_ENU�M�AMS�0�A ARGS, ty pename T�/��l�,(@J,I,D)�C@AT(T,ILa�@XA?V=PA,c��&MPTY�! 3�,a�	_TYPE�� \
 �2`1�"C�a&arg,I NC(I)),_yA<);X' hGR�EPE�0��F�#�(�]Com�eif  nonzero  numb@�of @uments@O � == 0  Dr��COM�MA�else�� ,"k@>�%"�C�s Ads �used in㚎v���a�coda�g%JOIN(���&��F@F~FI�NVOKER "FCJ_invoker_-*VOID�_�void_���
�OBJ_~�obj_U���^2�O����R$EF�T_;re�f��^�y��}Bu_O�p3�-hM`EMBER��m�em��OA��� ;��??�[GET�/}6get��//E_�QS�<�{�/)'�)���N�__"/��O"�)�)�Q���@�ao"o"�o"a"a/D#6�Q2���VTABLET�&basic_lvt�VMU@i�fn�
�NO_�,�RETURNS��om�h�y a-�v�(X) XP�p�OP�����::ӱ::e]::unus1�� X; �return���R�Ў (1"���Avpace r �{A�GS	S �x��U
�����<ó6���PtrP������ R=t���={��� �0�u@�ld*��P�c��*�� _buffer&� e_ptrO�_ �b���X B � f =�inter��t_c�ast<>W
@	�.
�	��G�!f���R ��}E}r�c[U�e�
�e�� ElfChnk h  c8x� t@�  � ` �\�C�8! v.3#k	�   ,  �E g	  T =�!�3��9�#��9������	*��	f�G?��1�  ��  Vm�%���M�F��)��#��'�$��� f =  reinterp ret_cast <Functio�nPtr>(f`_ptr.);
  BOO ST_FUNCT ION_RETU RN(f(�AR�GS)I}}��template<  typ@ename &OHbj,�R LC0OMMA
LTE MPLATE_PPARMS�	>s`truct�F9O BJ_INVOKERE{  s tatic R @invokeGQb�uffer& �U�objAT�.[C�2�B0)
 �" �M�* f�nif � allows_s
m _ ect_�optimiz�on&C 0>::v�alue��J�*>(&m.da�taKJelse���d*.� K�\ urn (*f)�2T�#TCT�WT
i�TTVOIDgq�T�T�I"�x_TYP�E�0�Y�Y#&� X)@�hXXonX��X�%X��ncrXL'$��VZ	`Z�?-OW1Y?-OW�a?-�OW�0�eF�OW�hOWsl_OW*�k��b�W +MSOW��+pv�, _w�hw�?�Lq�L��v�L  ��v�v���ve�m��vI��z�LR�E�7�L	�La1 �L�y�yN_?"�l��x�x �!?"��!�G���!�!	E�/BN_���"#0��NUM!b > 0�/*� Handle2�c� of me�mber poR��s. */%�����Mb ��'�']�
�'�'�'MEMBE�R�F�F�$�$�$�$A	�8 L?%:%��q�F-�[���r���JtA�b@oost:: _�fn�$�$_G_G/�#���	oooo�o�JC����F�c�F�a1 /"/"/"/"��!�h�Q ��F��  = reint erpret_c ast<Func tionObj*>(&f8_ob j_ptr.da�ta);
  else	  f��_Z �urn  (*f)(BO OST_FUNC TION_ARGjS	,}�} �t emplate<  typenPame �,R� 8COMMA�TEMPLAT�E_PARMS>struct!)VOID�hOB J_INVOKE
R{  static� RET URN_TYPE�invoke(��buffer&~ �M�9DyL;9),
�H* �V* "f
�if �$al�lows_sm �ect_opt�imiz ;on	� >::value@F� ����ǍR.�Ұ�Nk(��������_ZpZtREF�Y�R �T��T�B�V����[�
���@�t��  ���C���J��>~��?�g0D�D#&� ��?��_C���_C0_�LE#��NUM�� > 0�/* Handle�7c�V of  member poB�s. */%�t�tMbPtr�OGe �'�tx}�'�t+MEMBER��FV��s�Foq�$�oq�a	6 ?%MS�q����q��%��F�����  ��Fboost�:: _fn�f��_G 	ooo��oo�o��C��A�!��,��6������ri
���h࡟!� ��!�p �!u7���� �,tF�"|��� ���#endif��#�#�T�B"�?#?#?#?#ϐ	  ?#1#GE�c�c�def���: :if_c<(i s_void<R�6�xOy ��'v�4�(�/'��� �V�K�n
6X��I�>��\H�{� �"?�o#k# m9O��h�� �#&�����������1�1�1 Nt���MS
  >� struct B OOST_FUN CTION_VOID4REF_I@NVOKERl{  static�RETURN _TYPE^in voke(fun ction_bu@ffer& o@bj_ptrQCpOMMAF�$P�ARMS)
'�  F:Obj@* f = �'r einterpr et_cast<-�>Y�P.�);	#���((*f)�(ARGS)	
}�#}�(
#iBf�oNUM_ > 0/* H@andle �c��on of m ember poAjs. */�t emplate<  typen ame M�Pttr,R$LeL8T EMPLATE_�e��MEMBE�R_�R�;��D��1�ܔ�Kb�G>(&��`.dataK���u rn boost�::�t_fn֒ȡ���7�}��>�>�,b��?������_�C?C�gf 'i?C`�?C+Cl�E��#endi�f�9F��_���FF9�GE��W�a�defg���::if_ c<(is_vo id<R>::v�alue)��>� �LK�d�Q3'1���qf����)�>��(\��}� _��Sg���h � A##VUSy���1�%�P�(�v��?t  Ot��1OBJ�t����1��1�1n ��1x2��/���"���"=� 2Y\2_�Y� _�__��__=� _2U_2��_2�q��a_2/�q�&��R_2_2o� ��Á	_2_2_2�K ��[2x_2z/k� �"��"��"�"=� _2Y�\V2_Y� _��__�__=� _2_U_2�S2�_�  �Retrie ve the a�ppropri�4�r for a	<�. ;ߎ� BOOST_FU NCTION_C�OMMA
  �TEMPLAT�E_PARMS�>struct hMEMBER _INVOKERV{  sta tic R in voke(fun ction_bu@ffer& o�bj_ptra�����)
��E  Membe rPtr* f = �'rein terpret_ cast<�>(&�S.data);	! urn b oost::me m_fn(*f)B(RARGS�}"} '�temp late<   typenameuc,R��#�}VpOID_�~�R ETURN_TY�PE�0��E����7x��"C?(#�V�#e�ndifǺY�F�n�@_F_�_�O�GEH�c��\def�]�`: :if_c<(i s_void<R >::value�)�i?e�.BOơd�lQ3'1oq�q�A�c/>�
?
�?�?
�?o!����A�F"F�_�C��ObjH*�	"�=���8��*cOBJ����c�cB��1x�2:/�� ��� �l2O2���2_Y� _�_�_��	_� _2��_2�_2���a_2���q?��p_2ګZ28REF_2��_2pl�?d_2�
M �[2x_2��/�� �"�"��"���d�_2�\V2_�[� _�__��__=� _2U_2�S2#if�,NUMB_ > 0�^/ * Retrie ve the a�ppropri��4�r for aF  ��� pob�.�  */E����V���jep�<?��vE�j�O�q�8Ͽ��?8�?8�RO L28�7�ZOO_(_(��	��Z� �7�\��� ��� OST_FUNC TION_MEM BER_INVO@KER
  {   stati c R invo ke(funct ion_buffer&  obj�_ptr BO
��COMMA��-PARMS)
0�  Mem berPtr* f = Vrei nterpret@_cast<->(&�.data);	! urn  boost::m em_fn(*f�)(RARGS�}"} '�tem@plate<   typenamjec,R��FT EMPLATE_��'>stru�ct)VOID�~�RETURN�_TYPEE ���
���x��"C?(#��V�#endifǺ�Y�F�݀���_FpFGE���\def�]�`::if_c <(is_voi d<R>::va�lue)��?��.�BOf�dlQ3'1o�q+A�c/>��
?
?�?
��?o!&���A�F"F��_�C��ObjH*��	"�=��� ��*c�OBJ���c�c�(���c�?d"}+n ���}}�j2�2��\2_[� �#��__�__=� �_2U_2�_2����a_2e�L���_2a0�?�N_Z2REF�_2ԧ��_2?d?d�K ��[2x_2z/k� ��"�"��"=� _2�Y\V2_Y� _��__�__=� _2U_2�S2#if!m�NUM_qH > 0�z/* Re trieve t he appropri���r f`or a  ��� pob�.  */�E����V��jep�<��8�ۏ���߿Aߑ8Ϗߑ�֟jpl��j�M L28�7:O�-� _(_(��	?��� �7�\��� �����Bj��   >,
p0 BOOST_FU NCTION_M EMBER_IN@VOKER<�   MemberP�tr�  R e`COMMA[1T EMPLATE_PARGS9>>�::type 
;�}�#endif
�	/*  Given th�ag retu rned by  get_func tion_tagB,�riev he!��actua l invoker�%at wil�l be us &�fo�
e g�5�)� object. �L +Each  specializaC cont ains an  "apply" nest 2cla ss templ$at�'th�#acpcept� :  O��5@=�K, �'a�rgumentBs, d all ocator. T�resultxing�%�#�8�.tDwodef�"�M_k"�"ma`nager�I�wDhi�Dcor�p�ond t�@�^a��*. */� �F<Aname Tag�str�uct��GETE�8 { ��E��Re.t��F�zaQ p�ointe I�!�b<e_pt�E�-{��# F?�F�W�v� �vm!sPARMS��  D'�Q�(`��q�=�2B/�~�,&�_���c  �b� ߒ��d���AďG!��\ ���0�#�<or_�$X<> �bb��A
a IR_@�� _@j�7K@q�
A�E_a�G _E��_E�_EG_E��6e]� _EJ��g_E��S�"�C##if�NUMbw > 0���Ncn�Nm��oN�(� /Vq��?�ON�"�O\���-参�����/N�a/N���&/N�?��
� �??+_����Mo�N?+P�M��2 ��M��M��M����m�a v������M -0l�k�M;p��Md	��	/"/"�o �O/"�/"�/"N/"P/"�2 ��/"d�/"�!"� � }�� O� �L��?�<��K
q�K�����¦��m�KE~ObjO'H'�_,N_?�t?���wef?�N�6��OBJ��l �O_*�_*_*N�_*P_*�2 _*b�v�� *��)_l��o ��Gd � OOST_FUN CTION_ME MBER_INV OKER<
  MemberP�tr,NR B�`COMMAf1T EMPLATE_PARGS9>>�::type 
;%} 	#endif
�	/*  Given th�ag retu rned by  get_func tion_tagB,�riev he! �actua l invoker�%at wil�l be us &�fo�
e g�5�)� object. �L +Each  specializaC cont ains an  "apply" nest 2cla ss templ$at	Oth�Gacpcept� :  O��5@=�K, �'a�rgumentBs, d all ocator. T�resultxing�%�#�8�.tDwodef�"�M_k"�"ma`nager�I�wDhi�Dcor�p�ond t�@�^a��*. */� �F<Aname Tag�str�uct��GETE�8 { ��E��Re.t��F�zaQ p�ointe I�!�b<e_pt�E�-{��# �~�7FD���� �vm!sPARMS��  D'�Q�(`��q�=�2B/�~�,&�_���c  �b� ߒ��d���AďG!��\ ���0�#�<or_�$X<> �bb��A
a IR_@�� _@j�7K@q�
A�E_a�G _E��_E�_EG_E��6e]� _EJ��g_EȧIE**  �   :  �� #����  b_Y� &  � � 1   3     0  �� 1  
r " 6�  D Pp [ !P @ Zx @W[	�:�� D����ow�$)���	 M  i c r o s0 f t -D W0n d� wP- SPB CD lpe n0� Y��
�E��Ub'n2}_A_S m bZ/E� o�n e0tE�v0 t y@� ��x   �:�� V�w a0e  D N�t w�
r k� Apa p50
e ��t z8 ���q��p;tT�� �2 ps:�t]x  >���	0M _��CPn�ϧ��' �+1 p :� \ Dp�cp �\TB T _0 
c�i0 _ { �F 4 0P C�"2� - A�C �2p(40C 3� (A 9�A� 2 (1 7� BP 2 �E�2�F }r"0�u<t�.$��7�728�7�7  �7� s�	�0�7_<iY<�i�7�p�;p0{�=5��C�4�0PA�jD�-�4��9U�E� -pA0 Bհ9�8p0�sU�@su��:=t?���:�:�:�#" U8  [�:  ��`0�	U U_?�!uU�gZ Ue� Ut�<r�pVQX{U�
e \���7q >t[l'�7��7�7U�{�s�	p/�7u�  t - W i  n d o w s xS m bP C l �e dt / 4o $n eD c �i v lt  y ��� x        :      VD M �a r j T N 
t o k   A �a p5 Me  -_t (1  K s��  **  � ? �A(����� � b_Y�&  � |�  ��   �� 
� 	6�� <P�� ! a�  ]x @�8W[	�:� �D����ow��)�� �L�y�i��r��s�f�o1��M B�o���Y ��
�E�U�b'n2}M���c��o��o �"���ϧ��'�уӂt t: \ D�eAsc�\DiB TT _� c�gi� _@ { 9 F�,2Q�5 7@-�iCE@3@�4 E@8�@A�1 �2�7�0 5�A 2�{�7�2 } L��s��u�w@�uP(,�u_ 6Ae�� �u[�uZ��uE�u�  E&�u�u@�����d��o���oA�4���4��r�o�4U�4%�4%��4�TS��o�o�o��4�T  P DY�lo j`pn� �   !�m Vs��#s�t��*��2 c��2�h� �B�3�4�h�3���3 �h�3%�3%�3��3����- �����3�3���3C�3x�>��3�3�3[�%�3�	���Wp'��k�k�3�����}��3��p tPS5s�3�P���D�������Z 2 ����y�> �3�� � qh�	���/���������M sP��i �Mҩ�Eq���?�3�����K ����y�����	��� ���_�_8�gC0�np-�}��3�3��ҩ�F���H����3��g0 �g  �����	����Q�3�3�3�3�3�3�3�3�����R����G����Q�����������M���	��������0��+��Z��-�s Va���.�CP�AQ�6 4p�0�AQ�D F� 40BU��-��0P�B06� 6p�907 8� 0 8�} ��/�T����Ht�[RtU�   
    6 h D P 8[ !d[x @��[ R����W[ 	�:��D��@��ow�)��   H�M  i c r o s f t - �W &n d w * - S M B �C l e n # �Y��
�E �Ub'n2}Q#Em b
E/ oQ On e �t ]v t y � ��x  A �  : t � V�Fw aA�Ye   N�tD w�Ur k�AQ�[a p�Qe� ���/t 8 �y�i�  ** J �I���˃� Ab_Y�&| ���E� � F�  +M���Ӆ8�ӅL�<�i�i��io@e�i1��j�i� � J�iySB���i� ��1��Z��E�iE&�i�� j�4Q�i E`eh�hr��t b"�-�c� K�.��G�7����.�f�c �.��%�.  %��.�������������.��.�L�.��n��. �����; �.����]%� ̖ ��� �!H  %�.�.��Ǥ��.����d`���[�(�3��M�45��r�4�4��y�  D  �	�pW�v~~~߂ق�C�ynI~~�bi~  ~M�Ky���yNtC�k~�4 �2 ~�p���ys�	��� ��߂�>i�a��������r ����4q�r��4Ot��t�4����f�;O  y0��pO�	8߷�i�,�4׷�O����Ow��v�Pa�Ptd�~��4A0 y���\�q����  �	�_� �4�4�4ϧ��'V �`1 P6�O\� D���cp \�͠B T _0 cp�i0 _ { F �4 0P C��2� �-QD��2�4�ΈC 3� A 9��A� 2 1 7� �BP 20�3 2�xF }�r��7��pQt��$���/a�1;1 3Rb c��uR �z�7�t���H q�P m�	 ���_�-��� ?�?��r??�?S34��$sP��t��. h�qm�y��  P��q���_�R  ��� 7��  b_Y�&   �  !    �    |F  
ND  *  6   P [ ! �  Zx @	: � l'w �  ��w�o P B�  �2M i c@ r o s f@ t - W n d w - S  M B C l e n #�Y� �
�E�U b'n2}#Em *b�"/�o�'n *e�At�.v�t y ����x�  �T�: z�� V�Fw a�Y e   N�t �w�Ur k�A�[�a p�Qe� �1�/t 1 �{7 2���**  �E�S����׍��k\�kE�k�� E&�k �kA}�kϧ���'  �� �_� : \ D�gAuc�\DkB TT _� c�ii� _D {@�5 A@6@ 4 2 0@�AT 3@F@4�BQ@- 9@E��- �A� B@9 7 8�0@F }/�x�u�w�yT�w���w��%�q%�;�b�q}�qi`m�z�q�q�qd`u�q8��  y\*"�q��qU�5Z��5����1�%�5%�5�5�����5�5Q� E`sh�qr��Ae`!f�q�/�f�� V�0�T�����7���0"%�0�
|  %�0�0�0�0 ���wl{RvyWt�>�����!|1.1 ���11�������0[�i�  ��^�	��w��������_�o�~RsPaS �0Cp�n�r-c�-i �00&_�{�u�pIX�t�G����k{{I � q��	��iPE��y��ZtI-�:s�bt�b�.�FP0 F0��B�4A��C� �-�8p329� �A� 2�7� B��*2PN3�9�} �rE�5q�Mq Y��_���5�����y����	����p������_}��Ѹ��C�-P����Q��q:Z������� jf2 � u�� ��sR�	������PO�����WR{R�5uR�pR[�t��RRR��� UtR�js5�	qӳ  o f t -  W i n d �w s xS m@ b C l \eQ dt / 4o $n e c �i v lt y � ��x        4       P Yi p �j nn �  � k m s ��s t M* 2 9 F �  *H*  \   ����7�� vb_Y��&   pW~  �   � 
�    6�  T U !>Zx @	� �8 l'wB��H��w�7m�P !�L �HEM�ec r�zs#���M B���Y ��
�E�U�b'n2}�"�ӊo�i]�i�J��i_�iE�iE&�i �in�@�������c�i�@��^�i����4Bz�l��i `�i7�i%�4%��4`l+��ii ��r�iE��BV `s a�<a`�. h��m���  q0�!� �0�� �_�1c��1�1�����1%�1%�1�1����������1A� ���1���c`�1f-��c��1�1��c\��%�1���%�1�1�1����s�ϧ��'1�s�t�1: \, D�0Q4cp \ 
NP t�pT _ *T:p�p� { (9 Fp2p 5 "7P- 1�7 �3�4 E�8� *Ap 122�0 �52A 2 APj9p }`q��a��	����� ��O�P0Q��������  �  �	����И��O��_������A��o �/�y��ubt��������������y�s�	�p*����jW01�����n�����a����[������q ct�����4�4�4�}s�	����}_�o �����A���pW��� ���������������  NCTION_GET_INVOKER<member_ptr_tag>
      {
        template<typename MemberPtr,
                 typename R BOOST_FUNCTION_COMMA BOOST_FUNCTION_TEMPLATE_PARMS>
        struct apply
        {
          typedef typename BOOST_FUNCTION_GET_MEMBER_INVOKER<
                             MemberPtr,
                             R BOOST_FUNCTION_COMMA
                             BOOST_FUNCTION_TEMPLATE_ARGS
                           >::type
            invoker_type;

          typedef functor_manager<MemberPtr> manager_type;
        };

        template<typename MemberPtr,
                 typename R BOOST_FUNCTION_COMMA BOOST_FUNCTION_TEMPLATE_PARMS,
                 typename Allocator>
        struct apply_a
        {
          typedef typename BOOST_FUNCTION_GET_MEMBER_INVOKER<
                             MemberPtr,
                             R BOOST_FUNCTION_COMMA
                             BOOST_FUNCTION_TEMPLATE_ARGS
                           >::type
            invoker_type;

          typedef functor_manager<MemberPtr> manager_type;
        };
      };
#endif

      /* Retrieve the invoker for a function object. */
      template<>
      struct BOOST_FUNCTION_GET_INVOKER<function_obj_tag>
      {
        template<typename FunctionObj,
                 typename R BOOST_FUNCTION_COMMA BOOST_FUNCTION_TEMPLATE_PARMS>
        struct apply
        {
          typedef typename BOOST_FUNCTION_GET_FUNCTION_OBJ_INVOKER<
                             FunctionObj,
                             R BOOST_FUNCTION_COMMA
                             BOOST_FUNCTION_TEMPLATE_ARGS
                           >::type
            invoker_type;

          typedef functor_manager<FunctionObj> manager_type;
        };

        template<typename FunctionObj,
                 typename R BOOST_FUNCTION_COMMA BOOST_FUNCTION_TEMPLATE_PARMS,
                 typename Allocator>
        struct apply_a
        {
          typedef typename BOOST_FUNCTION_GET_FUNCTION_OBJ_INVOKER<
                             FunctionObj,
                             R BOOST_FUNCTION_COMMA
                             BOOST_FUNCTION_TEMPLATE_ARGS
                           >::type
            invoker_type;

          typedef functor_manager_a<FunctionObj, Allocator> manager_type;
        };
      };

      /* Retrieve the invoker for a reference to a function object. */
      template<>
      struct BOOST_FUNCTION_GET_INVOKER<function_obj_ref_tag>
      {
        template<typename RefWrapper,
                 typename R BOOST_FUNCTION_COMMA BOOST_FUNCTION_TEMPLATE_PARMS>
        struct apply
        {
          typedef typename BOOST_FUNCTION_GET_FUNCTION_REF_INVOKER<
                             typename RefWrapper::type,
                             R BOOST_FUNCTION_COMMA
                             BOOST_FUNCTION_TEMPLATE_ARGS
                           >::type
            invoker_type;

          typedef reference_manager<typename RefWrapper::type> manager_type;
        };

        template<typename RefWrapper,
                 typename R BOOST_FUNCTION_COMMA BOOST_FUNCTION_TEMPLATE_PARMS,
                 typename Allocator>
        struct apply_a
        {
          typedef typename BOOST_FUNCTION_GET_FUNCTION_REF_INVOKER<
                             typename RefWrapper::type,
                             R BOOST_FUNCTION_COMMA
                             BOOST_FUNCTION_TEMPLATE_ARGS
                           >::type
            invoker_type;

          typedef reference_manager<typename RefWrapper::type> manager_type;
        };
      };


      /**
       * vtable for a specific boost::function instance. This
       * structure must be an aggregate so that we can use static
       * initialization in boost::function's assign_to and assign_to_a
       * members. It therefore cannot have any constructors,
       * destructors, base classes, etc.
       */
      template<typename R BOOST_FUNCTION_COMMA BOOST_FUNCTION_TEMPLATE_PARMS>
      struct BOOST_FUNCTION_VTABLE
      {
#ifndef BOOST_NO_VOID_RETURNS
        typedef R         result_type;
#else
        typedef typename function_return_type<R>::type result_type;
#endif // BOOST_NO_VOID_RETURNS

        typedef result_type (*invoker_type)(function_buffer&
                                            BOOST_FUNCTION_COMMA
                                            BOOST_FUNCTION_TEMPLATE_ARGS);

        template<typename F>
        bool assign_to(F f, function_buffer& functor) const
        {
          typedef typename get_function_tag<F>::type tag;
          return assign_to(f, functor, tag());
        }
        template<typename F,typename Allocator>
        bool assign_to_a(F f, function_buffer& functor, Allocator a) const
        {
          typedef typename get_function_tag<F>::type tag;
          return assign_to_a(f, functor, a, tag());
        }

        void clear(function_buffer& functor) const
        {
          if (base.manager)
            base.manager(functor, functor, destroy_functor_tag);
        }

      private:
        // Function pointers
        template<typename FunctionPtr>
        bool 
        assign_to(FunctionPtr f, function_buffer& functor, function_ptr_tag) const
        {
          this->clear(functor);
          if (f) {
            // should be a reinterpret cast, but some compilers insist
            // on giving cv-qualifiers to free functions
            functor.func_ptr = reinterpret_cast<void (*)()>(f);
            return true;
          } else {
            return false;
          }
        }
        template<typename FunctionPtr,typename Allocator>
        bool 
        assign_to_a(FunctionPtr f, function_buffer& functor, Allocator, function_ptr_tag) const
        {
          return assign_to(f,functor,function_ptr_tag());
        }

        // Member pointers
#if BOOST_FUNCTION_NUM_ARGS > 0
        template<typename MemberPtr>
        bool assign_to(MemberPtr f, function_buffer& functor, member_ptr_tag) const
        {
          // DPG TBD: Add explicit support for member function
          // objects, so we invoke through mem_fn() but we retain the
          // right target_type() values.
          if (f) {
            this->assign_to(boost::mem_fn(f), functor);
            return true;
          } else {
            return false;
          }
        }
        template<typename MemberPtr,typename Allocator>
        bool assign_to_a(MemberPtr f, function_buffer& functor, Allocator a, member_ptr_tag) const
        {
          // DPG TBD: Add explicit support for member function
          // objects, so we invoke through mem_fn() but we retain the
          // right target_type() values.
          if (f) {
            this->assign_to_a(boost::mem_fn(f), functor, a);
            return true;
          } else {
            return false;
          }
        }
#endif // BOOST_FUNCTION_NUM_ARGS > 0

        // Function objects
        // Assign to a function object using the small object optimization
        template<typename FunctionObj>
        void 
        assign_functor(FunctionObj f, function_buffer& functor, mpl::true_) const
        {
          new (reinterpret_cast<void*>(&functor.data)) FunctionObj(f);
        }
        template<typename FunctionObj,typename Allocator>
        void 
        assign_functor_a(FunctionObj f, function_buffer& functor, Allocator, mpl::true_) const
        {
          assign_functor(f,functor,mpl::true_());
        }

        // Assign to a function object allocated on the heap.
        template<typename FunctionObj>
        void 
        assign_functor(FunctionObj f, function_buffer& functor, mpl::false_) const
        {
          functor.obj_ptr = new FunctionObj(f);
        }
        template<typename FunctionObj,typename Allocator>
        void 
        assign_functor_a(FunctionObj f, function_buffer& functor, Allocator a, mpl::false_) const
        {
          typedef functor_wrapper<FunctionObj,Allocator> functor_wrapper_type;
          typedef typename Allocator::template rebind<functor_wrapper_type>::other
            wrapper_allocator_type;
          typedef typename wrapper_allocator_type::pointer wrapper_allocator_pointer_type;
          wrapper_allocator_type wrapper_allocator(a);
          wrapper_allocator_pointer_type copy = wrapper_allocator.allocate(1);
          wrapper_allocator.construct(copy, functor_wrapper_type(f,a));
          functor_wrapper_type* new_f = static_cast<functor_wrapper_type*>(copy);
          functor.obj_ptr = new_f;
        }

        template<typename FunctionObj>
        bool 
        assign_to(FunctionObj f, function_buffer& functor, function_obj_tag) const
        {
          if (!boost::detail::function::has_empty_target(boost::addressof(f))) {
            assign_functor(f, functor, 
                           mpl::bool_<(function_allows_small_object_optimization<FunctionObj>::value)>());
            return true;
          } else {
            return false;
          }
        }
        template<typename FunctionObj,typename Allocator>
        bool 
        assign_to_a(FunctionObj f, function_buffer& functor, Allocator a, function_obj_tag) const
        {
          if (!boost::detail::function::has_empty_target(boost::addressof(f))) {
            assign_functor_a(f, functor, a,
                           mpl::bool_<(function_allows_small_object_optimization<FunctionObj>::value)>());
            return true;
          } else {
            return false;
          }
        }

        // Reference to a function object
        template<typename FunctionObj>
        bool 
        assign_to(const reference_wrapper<FunctionObj>& f, 
                  function_buffer& functor, function_obj_ref_tag) const
        {
          functor.obj_ref.obj_ptr = (void *)(f.get_pointer());
          functor.obj_ref.is_const_qualified = is_const<FunctionObj>::value;
          functor.obj_ref.is_volatile_qualified = is_volatile<FunctionObj>::value;
          return true;
        }
        template<typename FunctionObj,typename Allocator>
        bool 
        assign_to_a(const reference_wrapper<FunctionObj>& f, 
                  function_buffer& functor, Allocator, function_obj_ref_tag) const
        {
          return assign_to(f,functor,function_obj_ref_tag());
        }

      public:
        vtable_base base;
        invoker_type invoker;
      };
    } // end namespace function
  } // end namespace detail

  template<
    typename R BOOST_FUNCTION_COMMA
    BOOST_FUNCTION_TEMPLATE_PARMS
  >
  class BOOST_FUNCTION_FUNCTION : public function_base

#if BOOST_FUNCTION_NUM_ARGS == 1

    , public std::unary_function<T0,R>

#elif BOOST_FUNCTION_NUM_ARGS == 2

    , public std::binary_function<T0,T1,R>

#endif

  {
  public:
#ifndef BOOST_NO_VOID_RETURNS
    typedef R         result_type;
#else
    typedef  typename boost::detail::function::function_return_type<R>::type
      result_type;
#endif // BOOST_NO_VOID_RETURNS

  private:
    typedef boost::detail::function::BOOST_FUNCTION_VTABLE<
              R BOOST_FUNCTION_COMMA BOOST_FUNCTION_TEMPLATE_ARGS>
      vtable_type;

    vtable_type* get_vtable() const {
      return reinterpret_cast<vtable_type*>(
               reinterpret_cast<std::size_t>(vtable) & ~static_cast<size_t>(0x01));
    }

    struct clear_type {};

  public:
    BOOST_STATIC_CONSTANT(int, args = BOOST_FUNCTION_NUM_ARGS);

    // add signature for boost::lambda
    template<typename Args>
    struct sig
    {
      typedef result_type type;
    };

#if BOOST_FUNCTION_NUM_ARGS == 1
    typedef T0 argument_type;
#elif BOOST_FUNCTION_NUM_ARGS == 2
    typedef T0 first_argument_type;
    typedef T1 second_argument_type;
#endif

    BOOST_STATIC_CONSTANT(int, arity = BOOST_FUNCTION_NUM_ARGS);
    BOOST_FUNCTION_ARG_TYPES

    typedef BOOST_FUNCTION_FUNCTION self_type;

    BOOST_FUNCTION_FUNCTION() : function_base() { }

    // MSVC chokes if the following two constructors are collapsed into
    // one with a default parameter.
    template<typename Functor>
    BOOST_FUNCTION_FUNCTION(Functor BOOST_FUNCTION_TARGET_FIX(const &) f
#ifndef BOOST_NO_SFINAE
                            ,typename enable_if_c<
                            (boost::type_traits::ice_not<
                             (is_integral<Functor>::value)>::value),
                                        int>::type = 0
#endif // BOOST_NO_SFINAE
                            ) :
      function_base()
    {
      this->assign_to(f);
    }
    template<typename Functor,typename Allocator>
    BOOST_FUNCTION_FUNCTION(Functor BOOST_FUNCTION_TARGET_FIX(const &) f, Allocator a
#ifndef BOOST_NO_SFINAE
                            ,typename enable_if_c<
                            (boost::type_traits::ice_not<
                             (is_integral<Functor>::value)>::value),
                                        int>::type = 0
#endif // BOOST_NO_SFINAE
                            ) :
      function_base()
    {
      this->assign_to_a(f,a);
    }

#ifndef BOOST_NO_SFINAE
    BOOST_FUNCTION_FUNCTION(clear_type*) : function_base() { }
#else
    BOOST_FUNCTION_FUNCTION(int zero) : function_base()
    {
      BOOST_ASSERT(zero == 0);
    }
#endif

    BOOST_FUNCTION_FUNCTION(const BOOST_FUNCTION_FUNCTION& f) : function_base()
    {
      this->assign_to_own(f);
    }

    ~BOOST_FUNCTION_FUNCTION() { clear(); }

    result_type operator()(BOOST_FUNCTION_PARMS) const
    {
      if (this->empty())
        boost::throw_exception(bad_function_call());

      return get_vtable()->invoker
               (this->functor BOOST_FUNCTION_COMMA BOOST_FUNCTION_ARGS);
    }

    // The distinction between when to use BOOST_FUNCTION_FUNCTION and
    // when to use self_type is obnoxious. MSVC cannot handle self_type as
    // the return type of these assignment operators, but Borland C++ cannot
    // handle BOOST_FUNCTION_FUNCTION as the type of the temporary to
    // construct.
    template<typename Functor>
#ifndef BOOST_NO_SFINAE
    typename enable_if_c<
               (boost::type_traits::ice_not<
                 (is_integral<Functor>::value)>::value),
               BOOST_FUNCTION_FUNCTION&>::type
#else
    BOOST_FUNCTION_FUNCTION&
#endif
    operator=(Functor BOOST_FUNCTION_TARGET_FIX(const &) f)
    {
      this->clear();
      BOOST_TRY  {
        this->assign_to(f);
      } BOOST_CATCH (...) {
        vtable = 0;
        BOOST_RETHROW;
      }
      BOOST_CATCH_END
      return *this;
    }
    template<typename Functor,typename Allocator>
    void assign(Functor BOOST_FUNCTION_TARGET_FIX(const &) f, Allocator a)
    {
      this->clear();
      BOOST_TRY{
        this->assign_to_a(f,a);
      } BOOST_CATCH (...) {
        vtable = 0;
        BOOST_RETHROW;
      }
      BOOST_CATCH_END
    }

#ifndef BOOST_NO_SFINAE
    BOOST_FUNCTION_FUNCTION& operator=(clear_type*)
    {
      this->clear();
      return *this;
    }
#else
    BOOST_FUNCTION_FUNCTION& operator=(int zero)
    {
      BOOST_ASSERT(zero == 0);
      this->clear();
      return *this;
    }
#endif

    // Assignment from another BOOST_FUNCTION_FUNCTION
    BOOST_FUNCTION_FUNCTION& operator=(const BOOST_FUNCTION_FUNCTION& f)
    {
      if (&f == this)
        return *this;

      this->clear();
      BOOST_TRY {
        this->assign_to_own(f);
      } BOOST_CATCH (...) {
        vtable = 0;
        BOOST_RETHROW;
      }
      BOOST_CATCH_END
      return *this;
    }

    void swap(BOOST_FUNCTION_FUNCTION& other)
    {
      if (&other == this)
        return;

      BOOST_FUNCTION_FUNCTION tmp;
      tmp.move_assign(*this);
      this->move_assign(other);
      other.move_assign(tmp);
    }

    // Clear out a target, if there is one
    void clear()
    {
      if (vtable) {
        if (!this->has_trivial_copy_and_destroy())
          get_vtable()->clear(this->functor);
        vtable = 0;
      }
    }

#if (defined __SUNPRO_CC) && (__SUNPRO_CC <= 0x530) && !(defined BOOST_NO_COMPILER_CONFIG)
    // Sun C++ 5.3 can't handle the safe_bool idiom, so don't use it
    operator bool () const { return !this->empty(); }
#else
  private:
    struct dummy {
      void nonnull() {}
    };

    typedef void (dummy::*safe_bool)();

  public:
    operator safe_bool () const
      { return (this->empty())? 0 : &dummy::nonnull; }

    bool operator!() const
      { return this->empty(); }
#endif

  private:
    void assign_to_own(const BOOST_FUNCTION_FUNCTION& f)
    {
      if (!f.empty()) {
        this->vtable = f.vtable;
        if (this->has_trivial_copy_and_destroy())
          this->functor = f.functor;
        else
          get_vtable()->base.manager(f.functor, this->functor,
                                     boost::detail::function::clone_functor_tag);
      }
    }

    template<typename Functor>
    void assign_to(Functor f)
    {
      using detail::function::vtable_base;

      typedef typename detail::function::get_function_tag<Functor>::type tag;
      typedef detail::function::BOOST_FUNCTION_GET_INVOKER<tag> get_invoker;
      typedef typename get_invoker::
                         template apply<Functor, R BOOST_FUNCTION_COMMA 
                        BOOST_FUNCTION_TEMPLATE_ARGS>
        handler_type;
      
      typedef typename handler_type::invoker_type invoker_type;
      typedef typename handler_type::manager_type manager_type;

      // Note: it is extremely important that this initialization use
      // static initialization. Otherwise, we will have a race
      // condition here in multi-threaded code. See
      // http://thread.gmane.org/gmane.comp.lib.boost.devel/164902/.
      static const vtable_type stored_vtable = 
        { { &manager_type::manage }, &invoker_type::invoke };

      if (stored_vtable.assign_to(f, functor)) {
        std::size_t value = reinterpret_cast<std::size_t>(&stored_vtable.base);
        if (boost::has_trivial_copy_constructor<Functor>::value &&
            boost::has_trivial_destructor<Functor>::value &&
            detail::function::function_allows_small_object_optimization<Functor>::value)
          value |= static_cast<size_t>(0x01);
        vtable = reinterpret_cast<detail::function::vtable_base *>(value);
      } else 
        vtable = 0;
    }

    template<typename Functor,typename Allocator>
    void assign_to_a(Functor f,Allocator a)
    {
      using detail::function::vtable_base;

      typedef typename detail::function::get_function_tag<Functor>::type tag;
      typedef detail::function::BOOST_FUNCTION_GET_INVOKER<tag> get_invoker;
      typedef typename get_invoker::
                         template apply_a<Functor, R BOOST_FUNCTION_COMMA 
                         BOOST_FUNCTION_TEMPLATE_ARGS,
                         Allocator>
        handler_type;
      
      typedef typename handler_type::invoker_type invoker_type;
      typedef typename handler_type::manager_type manager_type;

      // Note: it is extremely important that this initialization use
      // static initialization. Otherwise, we will have a race
      // condition here in multi-threaded code. See
      // http://thread.gmane.org/gmane.comp.lib.boost.devel/164902/.
      static const vtable_type stored_vtable =
        { { &manager_type::manage }, &invoker_type::invoke };

      if (stored_vtable.assign_to_a(f, functor, a)) { 
        std::size_t value = reinterpret_cast<std::size_t>(&stored_vtable.base);
        if (boost::has_trivial_copy_constructor<Functor>::value &&
            boost::has_trivial_destructor<Functor>::value &&
            detail::function::function_allows_small_object_optimization<Functor>::value)
          value |= static_cast<std::size_t>(0x01);
        vtable = reinterpret_cast<detail::function::vtable_base *>(value);
      } else 
        vtable = 0;
    }

    // Moves the value from the specified argument to *this. If the argument 
    // has its function object allocated on the heap, move_assign will pass 
    // its buffer to *this, and set the argument's buffer pointer to NULL. 
    void move_assign(BOOST_FUNCTION_FUNCTION& f) 
    { 
      if (&f == this)
        return;

      BOOST_TRY {
        if (!f.empty()) {
          this->vtable = f.vtable;
          if (this->has_trivial_copy_and_destroy())
            this->functor = f.functor;
          else
            get_vtable()->base.manager(f.functor, this->functor,
                                     boost::detail::function::move_functor_tag);
          f.vtable = 0;
        } else {
          clear();
        }
      } BOOST_CATCH (...) {
        vtable = 0;
        BOOST_RETHROW;
      }
      BOOST_CATCH_END
    }
  };

  template<typename R BOOST_FUNCTION_COMMA BOOST_FUNCTION_TEMPLATE_PARMS>
  inline void swap(BOOST_FUNCTION_FUNCTION<
                     R BOOST_FUNCTION_COMMA
                     BOOST_FUNCTION_TEMPLATE_ARGS
                   >& f1,
                   BOOST_FUNCTION_FUNCTION<
                     R BOOST_FUNCTION_COMMA
                     BOOST_FUNCTION_TEMPLATE_ARGS
                   >& f2)
  {
    f1.swap(f2);
  }

// Poison comparisons between boost::function objects of the same type.
template<typename R BOOST_FUNCTION_COMMA BOOST_FUNCTION_TEMPLATE_PARMS>
  void operator==(const BOOST_FUNCTION_FUNCTION<
                          R BOOST_FUNCTION_COMMA
                          BOOST_FUNCTION_TEMPLATE_ARGS>&,
                  const BOOST_FUNCTION_FUNCTION<
                          R BOOST_FUNCTION_COMMA
                          BOOST_FUNCTION_TEMPLATE_ARGS>&);
template<typename R BOOST_FUNCTION_COMMA BOOST_FUNCTION_TEMPLATE_PARMS>
  void operator!=(const BOOST_FUNCTION_FUNCTION<
                          R BOOST_FUNCTION_COMMA
                          BOOST_FUNCTION_TEMPLATE_ARGS>&,
                  const BOOST_FUNCTION_FUNCTION<
                          R BOOST_FUNCTION_COMMA
                          BOOST_FUNCTION_TEMPLATE_ARGS>& );

#if !defined(BOOST_FUNCTION_NO_FUNCTION_TYPE_SYNTAX)

#if BOOST_FUNCTION_NUM_ARGS == 0
#define BOOST_FUNCTION_PARTIAL_SPEC R (void)
#else
#define BOOST_FUNCTION_PARTIAL_SPEC R (BOOST_PP_ENUM_PARAMS(BOOST_FUNCTION_NUM_ARGS,T))
#endif

template<typename R BOOST_FUNCTION_COMMA
         BOOST_FUNCTION_TEMPLATE_PARMS>
class function<BOOST_FUNCTION_PARTIAL_SPEC>
  : public BOOST_FUNCTION_FUNCTION<R BOOST_FUNCTION_COMMA BOOST_FUNCTION_TEMPLATE_ARGS>
{
  typedef BOOST_FUNCTION_FUNCTION<R BOOST_FUNCTION_COMMA BOOST_FUNCTION_TEMPLATE_ARGS> base_type;
  typedef function self_type;

  struct clear_type {};

public:

  function() : base_type() {}

  template<typename Functor>
  function(Functor f
#ifndef BOOST_NO_SFINAE
           ,typename enable_if_c<
                            (boost::type_traits::ice_not<
                          (is_integral<Functor>::value)>::value),
                       int>::type = 0
#endif
           ) :
    base_type(f)
  {
  }
  template<typename Functor,typename Allocator>
  function(Functor f, Allocator a
#ifndef BOOST_NO_SFINAE
           ,typename enable_if_c<
                            (boost::type_traits::ice_not<
                          (is_integral<Functor>::value)>::value),
                       int>::type = 0
#endif
           ) :
    base_type(f,a)
  {
  }

#ifndef BOOST_NO_SFINAE
  function(clear_type*) : base_type() {}
#endif

  function(const self_type& f) : base_type(static_cast<const base_type&>(f)){}

  function(const base_type& f) : base_type(static_cast<const base_type&>(f)){}

  self_type& operator=(const self_type& f)
  {
    self_type(f).swap(*this);
    return *this;
  }

  template<typename Functor>
#ifndef BOOST_NO_SFINAE
  typename enable_if_c<
                            (boost::type_traits::ice_not<
                         (is_integral<Functor>::value)>::value),
                      self_type&>::type
#else
  self_type&
#endif
  operator=(Functor f)
  {
    self_type(f).swap(*this);
    return *this;
  }

#ifndef BOOST_NO_SFINAE
  self_type& operator=(clear_type*)
  {
    this->clear();
    return *this;
  }
#endif

  self_type& operator=(const base_type& f)
  {
    self_type(f).swap(*this);
    return *this;
  }
};

#undef BOOST_FUNCTION_PARTIAL_SPEC
#endif // have partial specialization

} // end namespace boost

// Cleanup after ourselves...
#undef BOOST_FUNCTION_VTABLE
#undef BOOST_FUNCTION_COMMA
#undef BOOST_FUNCTION_FUNCTION
#undef BOOST_FUNCTION_FUNCTION_INVOKER
#undef BOOST_FUNCTION_VOID_FUNCTION_INVOKER
#undef BOOST_FUNCTION_FUNCTION_OBJ_INVOKER
#undef BOOST_FUNCTION_VOID_FUNCTION_OBJ_INVOKER
#undef BOOST_FUNCTION_FUNCTION_REF_INVOKER
#undef BOOST_FUNCTION_VOID_FUNCTION_REF_INVOKER
#undef BOOST_FUNCTION_MEMBER_INVOKER
#undef BOOST_FUNCTION_VOID_MEMBER_INVOKER
#undef BOOST_FUNCTION_GET_FUNCTION_INVOKER
#undef BOOST_FUNCTION_GET_FUNCTION_OBJ_INVOKER
#undef BOOST_FUNCTION_GET_FUNCTION_REF_INVOKER
#undef BOOST_FUNCTION_GET_MEM_FUNCTION_INVOKER
#undef BOOST_FUNCTION_GET_INVOKER
#undef BOOST_FUNCTION_TEMPLATE_PARMS
#undef BOOST_FUNCTION_TEMPLATE_ARGS
#undef BOOST_FUNCTION_PARMS
#undef BOOST_FUNCTION_PARM
#undef BOOST_FUNCTION_ARGS
#undef BOOST_FUNCTION_ARG_TYPE
#undef BOOST_FUNCTION_ARG_TYPES
#undef BOOST_FUNCTION_VOID_RETURN_TYPE
#undef BOOST_FUNCTION_RETURN

#if defined(BOOST_MSVC)
#   pragma warning( pop )
#endif       
