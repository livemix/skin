/*
-----------------------------------------------------------------------------
This source file is part of OGRE
    (Object-oriented Graphics Rendering Engine)
For the latest info, see http://www.ogre3d.org/

Copyright (c) 2000-2012 Torus Knot Software Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-----------------------------------------------------------------------------
*/

#ifndef __Animation_H__
#define __Animation_H__

#include "OgrePrerequisites.h"
#include "OgreString.h"
#include "OgreIteratorWrappers.h"
#include "OgreAnimable.h"
#include "OgreAnimationTrack.h"
#include "OgreAnimationState.h"

namespace Ogre {
	/** \addtogroup Core
	*  @{
	*/

	/** \addtogroup Animation
	*  @{
	*/

	class Animation;
	
	/** An animation container interface, which allows generic access to sibling animations.
	 @remarks
		Because Animation instances can be held by different kinds of classes, and
		there are sometimes instances when you need to reference other Animation 
		instances within the same container, this class allows generic access to
		named animations within that container, whatever it may be.
	*/
	class _OgreExport AnimationContainer
	{
	public:
		/** Gets the number of animations in this container. */
        virtual unsigned short getNumAnimations(void) const = 0;
		
        /** Retrieve an animation by index.  */
        virtual Animation* getAnimation(unsigned short index) const = 0;
		
		/** Retrieve an animation by name. */
		virtual Animation* getAnimation(const String& name) const = 0;
		
		/** Create a new animation with a given length owned by this container. */
		virtual Animation* createAnimation(const String& name, Real length) = 0;
		
		/** Returns whether this object contains the named animation. */
		virtual bool hasAnimation(const String& name) const = 0;
		
        /** Removes an Animation from this container. */
        virtual void removeAnimation(const String& name) = 0;
		
	};
    /** An animation sequence. 
    @remarks
        This class defines the interface for a sequence of animation, whether that
        be animation of a mesh, a path along a spline, or possibly more than one
        type of animation in one. An animation is made up of many 'tracks', which are
        the more specific types of animation.
    @par
        You should not create these animations directly. They will be created via a parent
        object which owns the animation, e.g. Skeleton.
    */
	class _OgreExport Animation : public AnimationAlloc
    {

    public:
        /** The types of animation interpolation available. */
        enum InterpolationMode
        {
            /** Values are interpolated along straight lines. */
            IM_LINEAR,
            /** Values are interpolated along a spline, resulting in smoother changes in direction. */
            IM_SPLINE
        };

        /** The types of rotational interpolation available. */
        enum RotationInterpolationMode
        {
            /** Values are interpolated linearly. This is faster but does not 
                necessarily give a completely accurate result.
            */
            RIM_LINEAR,
            /** Values are interpolated spherically. This is more accurate but
                has a higher cost.
            */
            RIM_SPHERICAL
        };
        /** You should not use this constructor directly, use the parent object such as Skeleton instead.
        @param name The name of the animation, should be unique within it's parent (e.g. Skeleton)
        @param length The length of the animation in seconds.
        */
        Animation(const String& name, Real length);
        virtual ~Animation();

        /** Gets the name of this animation. */
        const String& getName(void) const;

        /** Gets the total length of the animation. */
        Real getLength(void) const;

		/** Sets the length of the animation. 
		@note Changing the length of an animation may invalidate existing AnimationState
		instances which will need to be recreated. 
		*/
		void setLength(Real len);

        /** Creates a NodeAnimationTrack for animating a Node.
        @param handle Handle to give the track, used for accessing the track later. 
            Must be unique within this Animation.
        */
        NodeAnimationTrack* createNodeTrack(unsigned short handle);

		/** Creates a NumericAnimationTrack for animating any numeric value.
		@param handle Handle to give the track, used for accessing the track later. 
		Must be unique within this Animation.
		*/
		NumericAnimationTrack* createNumericTrack(unsigned short handle);

		/** Creates a VertexAnimationTrack for animating vertex position data.
		@param handle Handle to give the track, used for accessing the track later. 
		Must be unique within this Animation, and is used to identify the target. For example
		when applied to a Mesh, the handle must reference the index of the geometry being 
		modified; 0 for the shared geometry, and 1+ for SubMesh geometry with the same index-1.
		@param animType Either morph or pose animation, 
		*/
		VertexAnimationTrack* createVertexTrack(unsigned short handle, VertexAnimationType animType);

		/** Creates a new AnimationTrack automatically associated with a Node. 
        @remarks
            This method creates a standard AnimationTrack, but also associates it with a
            target Node which will receive all keyframe effects.
        @param handle Numeric handle to give the track, used for accessing the track later. 
            Must be unique within this Animation.
        @param node A pointer to the Node object which will be affected by this track
        */
        NodeAnimationTrack* createNodeTrack(unsigned short handle, Node* node);

		/** Creates a NumericAnimationTrack and associates it with an animable. 
		@param handle Handle to give the track, used for accessing the track later. 
		@param anim Animable object link
		Must be unique within this Animation.
		*/
		NumericAnimationTrack* createNumericTrack(unsigned short handle, 
			const AnimableValuePtr& anim);

		/** Creates a VertexAnimationTrack and associates it with VertexData. 
		@param handle Handle to give the track, used for accessing the track later. 
		@param data VertexData object link
		@param animType The animation type 
		Must be unique within this Animation.
		*/
		VertexAnimationTrack* createVertexTrack(unsigned short handle, 
			VertexData* data, VertexAnimationType animType);

		/** Gets the number of NodeAnimationTrack objects contained in this animation. */
        unsigned short getNumNodeTracks(void) const;

        /** Gets a node track by it's handle. */
        NodeAnimationTrack* getNodeTrack(unsigned short handle) const;

		/** Does a track exist with the given handle? */
		bool hasNodeTrack(unsigned short handle) const;

		/** Gets the number of NumericAnimationTrack objects contained in this animation. */
		unsigned short getNumNumericTracks(void) const;

		/** Gets a numeric track by it's handle. */
		NumericAnimationTrack* getNumericTrack(unsigned short handle) const;

		/** Does a track exist with the given handle? */
		bool hasNumericTrack(unsigned short handle) const;

		/** Gets the number of VertexAnimationTrack objects contained in this animation. */
		unsigned short getNumVertexTracks(void) const;

		/** Gets a Vertex track by it's handle. */
		VertexAnimationTrack* getVertexTrack(unsigned short handle) const;

		/** Does a track exist with the given handle? */
		bool hasVertexTrack(unsigned short handle) const;
		
		/** Destroys the node track with the given handle. */
        void destroyNodeTrack(unsigned short handle);

		/** Destroys the numeric track with the given handle. */
		void destroyNumericTrack(unsigned short handle);

		/** Destroys the Vertex track with the given handle. */
		void destroyVertexTrack(unsigned short handle);

		/** Removes and destroys all tracks making up this animation. */
        void destroyAllTracks(void);

		/** Removes and destroys all tracks making up this animation. */
		void destroyAllNodeTracks(void);
		/** Removes and destroys all tracks making up this animation. */
		void destroyAllNumericTracks(void);
		/** Removes and destroys all tracks making up this animation. */
		void destroyAllVertexTracks(void);

        /** Applies an animation given a specific time point and weight.
        @remarks
            Where you have associated animation tracks with objects, you can easily apply
            an animation to those objects by calling this method.
        @param timePos The time position in the animation to apply.
        @param weight The influence to give to this track, 1.0 for full influence, less to blend with
          other animations.
	    @param scale The scale to apply to translations and scalings, useful for 
			adapting an animation to a different size target.
        */
        void apply(Real timePos, Real weight = 1.0, Real scale = 1.0f);

        /** Applies all node tracks given a specific time point and weight to the specified node.
        @remarks
            It does not consider the actual node tracks are attached to.
			As such, it resembles the apply method for a given skeleton (see below).
        @param timePos The time position in the animation to apply.
        @param weight The influence to give to this track, 1.0 for full influence, less to blend with
          other animations.
	    @param scale The scale to apply to translations and scalings, useful for 
			adapting an animation to a different size target.
        */
		void applyToNode(Node* node, Real timePos, Real weight = 1.0, Real scale = 1.0f);

       /** Applies all node tracks given a specific time point and weight to a given skeleton.
        @remarks
        Where you have associated animation tracks with Node objects, you can easily apply
        an animation to those nodes by calling this method.
        @param timePos The time position in the animation to apply.
        @param weight The influence to give to this track, 1.0 for full influence, less to blend with
        other animations.
	    @param scale The scale to apply to translations and scalings, useful for 
			adapting an animation to a different size target.
        */
        void apply(Skeleton* skeleton, Real timePos, Real weight = 1.0, Real scale = 1.0f);

        /** Applies all node tracks given a specific time point and weight to a given skeleton.
        @remarks
        Where you have associated animation tracks with Node objects, you can easily apply
        an animation to those nodes by calling this method.
        @param timePos The time position in the animation to apply.
        @param weight The influence to give to this track, 1.0 for full influence, less to blend with
        other animations.
        @param blendMask The influence array defining additional per bone weights. These will
        be modulated with the weight factor.
        @param scale The scale to apply to translations and scalings, useful for 
        adapting an animation to a different size target.
        */
        void apply(Skeleton* skeleton, Real timePos, float weight,
          const AnimationState::BoneBlendMask* blendMask, Real scale);

		/** Applies all vertex tracks given a specific time point and weight to a given entity.
		4� x ( 3 4 1 )   E S N T [@ 1 0 .11 |3 9 �<]   R �T A �I LT M �M B C � � R T�  *H*  �0� +.�7��  ��@K&          �: 	 
'#=!M�� � y�H E w�)C  Q�� �S  e a r cD h�xn d�x�r�4��6 0��P V :����V ���G��E�"0U�$E��R�-�38�1 (�<r o��H.�I��: 2��0 5��@c pm�Xg��(�����y�yB�Be E$�yB��y  �y�y3�{�y��� �y�<"pfy�az�y�� �y  %��<�y��<S>~�<�y� �|�yE �����.��<�<�< �z����<  %�<�<�<ట�<�<�<�<��0 �y�a<a |�y�y�yx �y	 ��y�y�y�w4pq���y�.w�ye�y�y�3P��y�y�����S�| ��� 	��� ������[��[�9'�<����|!��x �	Q�������E����x�~" �	_���� 0=pQ~  ����7��=  � F i lp�*s��c0�p� d   
A�
�]S� '� ElfChnk h#�  �8x� t��  �� ` 8P���8! v�-�	��   ;  �X  � 	c = 	AD  �k�-�%��1g��	�M�	�G���c��  i��MՀ��!Y�#&�#���y**  ��:�{�+.�7� � 0��KE'�}  5_}Vm���K@9A	A ?�8.�  E v e n Jt �� jD8�  x m l s 5 h�	t p : /Q@ s c@e�aP s .�i@rD o@o f�
.�o�/ w iA�/ 2 0@ 4Q@0 8@e�s�J���@"�m�oT S y@
t�+m�#A��H�Et�{ P� Av�d@	r@
%� �K� (N a $e�A?S@@N T 0A M@��a��FI D�'� A�� )�
 �Q u�l�f� e@AJ � @��
��d� L &A\l�  p  AҀ XE{�#ATk�D� $ ARj� K�y�*o�d�"  � �"P@�0D;��	iB�C@e�!(d`���\A><{

,T m`   
 <G}F�#R�c�>r d #%
 
�`88�a��  �@*jC Fa An@B � A@Jp M�GJa�Fi`	n �B�Aq�;n Cu m�u��-�T�3E�K OQ`@- G�S Ey�=B a"	���T.�e`Su "iU@	y�` E�fpL U�*a#   ! @  a �3 e  p  ��: q`  
��� �$!�	�� �{%�~ �}w�)C�a��-Q� � �/�x?`��/A,���D��	h6DAa �T�P����o *�0k�`T�aT `��!P� B Dn�r�r�O  �!��a	�,�a�,!�I�>�rx`?as4 1 6 0Q�P V��  0 Ma 1 4 3 j9` .`  	=GE�kE�0  @RT R�-�
8` 1� ("a�r�c��
x 2`0 5 �)�c�Ka g U��x`(�4�1m  �
E�[`
�]U�R T@[I� U T�� ` B�XS�b� ���0$���NO'f�  =oO'M'	L'�Or� 5h�?nP@e`�9�OOO1 a. �k OO�O30O�O)��D�CT O��
OR HM�M�1�C +�" e k��u%�{F�E��E�EEu �E	} �<�:�<:��3�?  \L 4�1�<�<U: ��<�Q)�<1(���y&OdOd��x 	O[� bPO[��O[�<�<��<�<p�h�<u�<APLL�)�r� y'��� 	��yp �y�]��<�y��y�y�y(�y@�[�
��<� z(�x '� � �+.�7�H�  (�@  w�)@C   T@�  |S  e a r c  h I n d x r :4 1 6 0NP V :   "0 . &. 4 3 9#   S!G (L EE0 IE VR S- K8 1D (yr o �.Q �x x /2 c0 5 );c p- �g )!( 4 �V)�UE�DE N� T [�)�V]�?�R�ET A�zL�T M�M B C� z   �逩**�)����� ��K&    �� � �  ��� �� 
 �#
=!�&�w��H �y�y N �yA��y0 �y*��y@|�y��E$�ybI�<w�y�<�y �|�y�y �`�E`~�y�y+�<"p~fyaz�yb,� �y � %�<�� �<�<�<o�<�<���ySF.�,w�<�< �z���< ~ %�<�<�<�m1�0s|Rw�y�y�y�ep�c�{xP}3���y�yՄ[R�=�y�=�y-���y�< ��y�yF �y	~ ������s1�R����1�� ЕS0 ���
�Dz[.��5�[�<�� 	Э�� �02��_3�����<��<e���3�#����?������#r �.�</��[�+� 	��:� ��Pϓ�': �a �[[[�{�|0� �	���>����	� 8 1 ( e r o 0. c x : 2 3  0 5 ) :   Tp a g� ��( |4 4 ~�)   E S  N T [ "0 �. v. 4 J9 ] �R JT A I LT M� M B C A Hr .�  �**  1� �+.�7��  ����K&          : 	 
G'=!�&�r� �<��H Ew�)C � �� ��S��a��c h�x�n d�x�r���6 0�P Vm��1��� ���G��E�"0��E R;� ��-�����y0 �0�y2�y�yB�Be �E$�yB�y  �y��3�{�y�� R T��<3�<"pfyaz�y��� �y  %�<���r���y�,�y7�<��s����0 �<.�<4w�<�< �z���< � %�<�<��yS>���<�<�<�< d[��|5�y�y�y��< �y	 �y��<�y�w���y��[�y}x�|����3� ��{�|6� 	�� A����q��<RR�������e�v�(P������|7� 	�P���y�����3P�����<�<�<x�8� 	 �0@??�����E0�x9r� �K&       0    T  t  t 
�P=!! V �@�  �+.�7��	"9�  w�)C�  1�j  S e a r  c h I n *d x r 4  1 6 0& P@ V :   0 . . 	4 3� 9   S!�G L E�"0�$XE R� �)-�%8�1 (�<r oE�H.�Ix x�2A�10 5 )�c� p�Xg��(�T4 �V)�UE�DE� N T [�)�V�]�?R�ET A�z"L�T M�M 8B C� z R �T耩**  ��:���������E$� �y�<�y  �yA�i�y0 �y;�y@��� �y  �y  %�<�y =�<�y�<�y נ|�y�y `�E`~�y��y<�<"�f�a����Aio �<  %�<�<� �<�<�<�<�<���yvSf.�=�<�< � z���<0?	 �y��y�y�y�yqU1�{qq�10s|Rw�y�yU�yep�c�{xP}3���y�yՄ[��y��|>���y�<���%� �	P��� ��_3r�����e��3�#�����Q���|?�����<��i 	��:�� ?�а��: ��a ����<�<�<�y����@�� �	_��>�����y���(���
�<�<s�<A�~ 	P[�<�  6 0  P �V :   1 x.1 4 3 F9 <  S�GP L E�0 �E� R �- �8 �1 ( e r �o . c x : 2 c0 5 );c p a Vg )!( 4 �1 !  aS N T� [ S0 �] �R %T A I �"  �T M M 8B C �� R  T�  **  �B��+. �7��  ��K&    B �  �@   �� 
�
H=!�&��. �<��H Ew�@)C  �T� �S��a��c h�xn dՀx�r�4������y�zK��yC�y�yB�?Bo E$�yB��  ��y�y �|�yE �y�y�yD�<�0��y�y�m�+� �y%E�%�<��r�'�y��;w�<�<�<E���<!�Ew`����E�<`7����z���<��$�<�<`G��y���<x0y�y�y��y�yׄ[p;�yp�<�F�y�y�y�- �� �<	 �y�<�y���<��R���e����o�<�y[� �y�G� �	� ������?�������y3�
Q�)�[���<�<s�<H��Z 	�_Ӱ�:[�;4��[[�(տ��
|I�� �	P��<_Y� ��??�<�<�<�<�<�<��� 0 .01 40 3 9 0x]    R E T  A I L4T M M B C S  R T�@  **  J�0�0�7� �  ��K&  B 8      h : 	 
'=!�M� � y� �w�)C�  1� >�S e a  r c h�xn *d�x�r�4  1 6 0�P hV :��1��� ����G��E�"0�$�E��R�-�38��1 (�<r o�H�.�Ix x�2�� 0 5 )�c Zp�Xg��(�4U��1� ��S�N��[�)��K�y�yB�?Be E$�yB�y  ��yA��y0 ��L�<�"pfyaz�yb,� �y|  %�<�y �<S7>~�<�y �|�yE ��y���.�M�<�< ��z���<  %�<�<�<��<�<�<�<Eo ��<��< `���N��<0�y�yY ��y	 �y�y�y�w{���y.�q�ye��y�y3P��y�y)P�1z��<�~O�����<	� �	�� �����q�:� �a �Q��
��
�y�Y���|[P��i �	�����y30)�����x�Q� �	_��� ��P�?��9d�<�<s��<Rp� !   �  � �0ѐ7���R�$  w�@)C  �� �>S e  a r c h@ I n d x r :4 1 6 0NP V :D   "0 . &.� 	4 3 9   S!G L �EE0 IE R S- K8 1 (Qyr o �. �x x /2 c0 5D );c p �gK��(�4 �V)�UE�DE N T� [�)�V]�?R�E(T A�zL�T �M�M B C� Cz R T耩*�*  �S����� ��K&  
 ��  �   *� �� �
���	=��E$ �y�!�y  �y�A��y0 �yT�y�x��y��E$�y�M�<�y�<]�y �|�y�y `�E�`~�y�yU�<�ybz�y�  �y  %�<��� =�<�<�<�<�<���y�Sb[� �V�<�<�<���<  %�<�<`�1q�y ?q�s|�Rw�y�y�yep��c�{xP}3��y�y�Մ[��yp|W����y�y�y�y�* ��	 �������s1R�� �e��30�����1'|X����1��� �	��� 02���_3�����: ��a ���<��<�<�y����Y���И�<=	��:[[������y���(��P+�
�<�<C�<Z�M� �	P�<��<�<�<�@   E R :A �- 1 8 1@ ( e r oA . c x :  2 3 0 5 ) �c p Xa g RB( >4D 4 �)   �SA N T [ "0T . v. 4 J9+ ] �R %T  A I LT M M B C S GR T�@  **  [��0�7� �  ��K&  B       h : 	 
�
=!ɂ&�� �<��H Ew�)C�  �� *�S��a��c �h�xn d�x�Fr���6 0�P� V��1��� ��՝G��E�"0�����E���y\�y�yB�Be� E$�yB�y  �yo�y�y�y�y]�<"p?fyaz�yb,� �y  �%�<��r`��y�,�y77�<��  ��E ������.�^�<�< ��z���<  %�<��< �yS>��<�<�<�E ��< ��[ 0z��y�y_�y�y�y�Q%< �y	 �y��<�y�w���y��[�}x�|����3� ��yx`� 	�� ����o�<RR��������e���(P��������a� 	�P���y�����y30)����<�<�<�<b� 	 ���??�����y�y|�ct� �0�7��  �� �K&           T  t  t 
N(=!! V �@� 3�c�	�w�@)C  1�j S e  a r c h �I n d x r 4 1 6 0& P V :D   0 . .� 	4 3 9   S!G L �E�"0�$E R� �)-�%8�1 (Q�<r o�H.�Ix x�2�10 5D )�c p�XgK��(�4 �V)�UE�DE N T� [�)�V]�?R�E(T A�zL�T �M�M B C� z R T� � **  �d��w��(E$ �y��y N �yA��y0 �ye��y {` �y  �y�  %�<�y =�<�y��<�y �|�y�y `�nE`~�y�yf�<"�?f�a���Aio �<  �%�<�< �<�<�<�<��<���ySf.�g�<��<  z���y	� �y�y�y�y�yqUv1�{qq10s|R�w�y�y�yep�c�{�xP}3��y�yՄ[���y�|h�����%� 	wP�� ��_3�r����e���3�#����Q���|i��� 	��:�� ?�аO�: ��a� ���<�<�<�y�����j�� 	_���>�����y����(���
�<�<s��<k� 	Pt[�@ d e x 0r    4 1 6 0 H  P V :   �0 . L. $4 3 9# <  S�G (L E�0 �E VR �- �8 1 (�r o rU ec �x /2 c0 5 );c p� a g ). !�( 4�) �E ��E N T [ S�] R �T A I �  �T M� M B C� z R T�  �**  �l� �0�7��  ����K&     �  �   �� 
G�
=!�&�r� �<��H Ew�)C � �� ��S��a��c h�x~n���"�y�y�Q�ym��y�yB�(E$�yB���  �y�y `x�y�<�!xE`~�y�yn�<"p~fyaz�y�6� �y � % �y�y�����<�������S ������.��yo�<�< �z����<  %�<��r�d����,1 ��5�y�<��y(�y�y�{ׄ�y���y�|p�y�y��yQ%< �y	� �y���� ���R�����e���[1���y�~q �	 �1����մ�: ����������<��<s�<r� 	��_Y�;4�y��[[[([[�<��<�<�<s� 	�P� ?�P��[[e��3P�����) �@   E S 0N  T [ 1 0 .1 4 3 9 <]   R �T A I "L4T M M 8B C � �R  T�  **  t�0�0 �7��  ��K&   8 B   @   :� 	 
'H=!M� �. y� �w�@)C  �� �S e  a r c hQ�xn d�x�rE�4��6 0�P� V :���� ��U�G��E�"0�$EU��R�-�38�1D (�<r o�H.Q�Ix x�2��0 5 )�c p��Xg��(�4���1���u�y�yB�Be� E$�yB�y  �y�A��y0 ��v�<"p~fyaz�yb,� �y � %�<�y �<S>~��<�y �|�yE ��w���.�w�<�< �z���<  %�<�<��<��<�<�<�<���y�SA<b �<x�y�y��y&� �y	~ �y�y�y�w���y.ow�ye�y�y3�P��y�y)0�����<� 0�z�y��� 	��� ����?�: ��a �Q��
������<��z� 	�������y30)�����x�{� 	�_�� �Z�?4?�9d��px���y|?r'� 
 =!   ��� ��0�7��D|�$  w�)C � b� |> S e a r  c h I n *d x r :4  1 6 0NP  V :   0 
. . 	4 3 F9   S!GP L EE0 IE� R S- K8 �1 (yr o ��. �x x /2 c 0 5 )�c Zp�Xg��(�4* �V)�UE�DE `N T [�)�V]E�?R�ET A�zL�T M�M B C� z R TB耩**  �}�������K&P   ��  � P  � �р 
���	;��E$ �y�!�y  ��yA��y0 �y~�y��x�y��%�<�y =�<w�y�<�y �|�y�y �`�E`~�y�y�<�y�bz�y  �y  %��<�<�< ��<�<�<�<����yS C 춀�<�f�2���������
�<%%�<�<0��<1q�y ?q�s|�Rw�y�y�yep��c�{xP}3��y�y�Մ[��yp|�����y�y�y�y�* ܀ }	 �������s1R�� �e��30�����1'|�����1���� 	���� 02��_3����': ��a ���<�<�<�y��������И�<=�	�:�[[�����y����(��P+�
�<�<C��<�M�� 	P�<��<�<��  0   G L0 E : ��E R h- 1 8 1 ( e r o . c xA : 2 3 �5 )vc p a, g RB( >4 �4 �)   �S  N T [ "0 �. v. 4 %9 ] �R %T A I �  �T M M B C S GR T�   **  ��f�2�7��  � �K&  !       4 �� �
�
=!�&�� �<��H Ew�)CD  �� �S��a��c hQ�xn d�x�r#���6 0�P �V��1��� �������y��y�yB�Be E$�yB�y���y�y3��{�y�� �y��<"p~fyaz�y�� �y � %�<��r`��ya�o�y7�y��  ��E� �����.�<��<�<� �z���<  %��<�< �yS>��<�<-�<Ed1�{3 �[�y�y�y��y�y��yQ%< �y	� �y�<�y�w���y���[�xP��������z��� 	�� ����q��<R�������0 �����y�
�
�y��Y���|[�� 	�_Y�P����y3����[[[x[�� 	��9�3��[[[[��(P�[[[[[r[� **  �  ��  f�2� 7��  ��K&   l  !   �  r q 
N(=$!��@� y� �w�) C  1
� >)S e  a r c h �I n d x r 4 1 6 0'P V : �  0 . . 	`4 3 9��  S�G L Eł"0�$E R� �)�-�%8�1 (�<(r o�H.�Ix 
x�2�10 5 �)�c p�Xg���(�4 �V)�UE�DE N T V[�)�V]�?R�ET A�zL�T M��M B C� z� R T�����?D�(E$�yB�y  ��yA��y0 �y��y��yE8 �y  %�<��y �<�y�<�y �|��y�y `�E`~�y�y���<"�f�a���F;�
�<  %�<�<��<�<�<�<�<���yS��-춑�<�< �z����y	 �y�yo�y�y�yqU1�{qq1�0s|Rw�y�y�y�ep�c�{xP}3���y�yՄ[��y�|������%� 	P�� ���_3r�����e��3�#����Q��|���� �	�:��� ?�а��: ��a ���<�<��<�y��������� 	�_��>������y���(տ��
�<�<s�<� 	y�  S e a r  c h I n d xx r    4 1 6 0 $  P V :D   D0 . L.� $4 3 9   �BG L E��0 �E R S�- K8 1 (y(r o �. �x 
x 